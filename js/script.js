var MINI = require('minified');
var _=MINI._, $=MINI.$, $$=MINI.$$, EE=MINI.EE, HTML=MINI.HTML;

var wsHost = "msg.alpinelinux.org"
var wsPort = "443"
var wsSub = "build/#"
var max_mqtt_msgs_count = 3
var buildlogsUri = "http://build.alpinelinux.org/buildlogs"
var lastmsg = {}

function connect() {
	// Create a client instance
	var clientId = Math.random().toString(36).substr(2, 22); // length - 20 symb
	var client = new Paho.MQTT.Client(wsHost, Number(wsPort), clientId);

	// set callback handlers
	client.onConnectionLost = onConnectionLost;
	client.onMessageArrived = onMessageArrived;

	// connect the client
	client.connect({useSSL:true,onSuccess:onConnect,onFailure:onFailure});

	// called when the client connects
	function onConnect() {
		// Once a connection has been made, make a subscription
		client.subscribe(wsSub);
		echo('CONNECTED', false, 'green');
	}

	function onFailure() {
		echo('Sorry, the websocket at "' + wsHost + '" is unavailable.', false, 'red');
		setTimeout(connect, 2000)
	}

	// called when the client looses its connection
	function onConnectionLost(responseObject) {
		if (responseObject.errorCode !== 0) {
			echo('CONNECTION LOST<br>' + responseObject.errorMessage, false, 'red');
			setTimeout(connect, 2000)
		}
	}

	// called when a message arrives
	function onMessageArrived(message) {
		var host = message.destinationName || null;
		if (host == null) return
		var a = host.split("/");
		var err = false;
		if (a[2] == "errors") {
			err = true;
		}
		host = a[1];
		if (host.includes('mips')) {
			return
		}
		if (host && host.match(/^build-/))
			mqtt_msg(host,message.payloadString,err);
	}

	setTimeout('sort_table();', 1500);
}

function echo(msg,mqtt_msg,color){
	var pre = document.createElement("p");
	pre.style.wordWrap = "break-word";
	if (color) pre.style.color = color;
	pre.innerHTML = msg;

	var output = (mqtt_msg ? document.getElementById("mqtt_msgs") : document.getElementById("mqtt_connect_status"));
	if (output) output.appendChild(pre);

	var max_msg_count = ( mqtt_msg ? max_mqtt_msgs_count : 1 );
	if (output && output.childNodes.length > max_msg_count) {
		output.removeChild(output.firstChild);
	}
}

function get_id(host) {
	return "bs_"+host;
}

function parse_version(host) {
	const ver = host.split('-').slice(1, 3)
	if (ver[0] === 'edge') return [4, 0]
	return ver.map(Number)
}

function add_server_row(host) {
	var id = get_id(host);
	const table = document.querySelector('#servers')
	const template = document.querySelector('#buildserver')

	const row = template.content.cloneNode(true).querySelector('tr')
	row.id = id
	row.querySelector('.host').textContent = host

	const versionA = parse_version(id)

	const before = Array.from(table.children).find(child => {
		const versionB = parse_version(child.id)

		// compare major
		if (versionA[0] !== versionB[0]) {
			return versionA[0] > versionB[0]
		}

		// compare minor
		if (versionA[1] !== versionB[1]) {
			return versionA[1] > versionB[1]
		}

		// sort arch alphabetically
		return child.id.localeCompare(id) === 1
	})

	if (before) {
		table.insertBefore(row, before)
	} else {
		table.appendChild(row)
	}
}

function builderror_msg(host, msg) {
	var id = get_id(host);
	var errmsg = '';
	if (msg) {
		var obj = JSON.parse(msg);
		errmsg = EE('a', {'href': obj.logurl, 'className':"errmsgs"}, obj.reponame+"/"+obj.pkgname);
	}
	return $('#'+id+' .errmsgs').replace(errmsg);
}

function mqtt_msg(host, msg, err){
	var id = get_id(host);
	if (!$('#servers #'+id).length) {
		add_server_row(host);
	}

	if (err) {
		return builderror_msg(host, msg);
	}

	if (msg == lastmsg[host]) {
		return;
	}
	lastmsg[host] = msg;

	if (msg == "idle") {
		$('#'+id+' .msgs').ht(''); // clear previous messages
	}else if ($('#'+id+' .msgs span').length >= max_mqtt_msgs_count) {
		$('#'+id+' .msgs span')[0].remove();
		$('#'+id+' .msgs br')[0].remove();
	}

	var pat = /^(\d+)\/(\d+)\s+(\d+)\/(\d+)\s+(.*)/i
	if (msg.match(pat)) {
		var msg_arr = msg.match(pat);
		var [, built_curr, built_last, repo_curr, repo_last] = msg_arr
		// var built_curr = msg_arr[1];
		// var built_last = msg_arr[2];
		// var repo_curr = msg_arr[3];
		// var repo_last = msg_arr[4];


		msg = EE('a', {'href': buildlog_url(host, msg_arr[5])}, msg_arr[5]);

		document.querySelector(`#${id} .prgr_built`).textContent = `${built_curr}/${built_last}`
		document.querySelector(`#${id} .prgr_total`).textContent = `${repo_curr}/${repo_last}`
	} else {
		document.querySelector(`#${id} .prgr_built`).textContent = ''
		document.querySelector(`#${id} .prgr_total`).textContent = ''
	}

	$('#'+id+' .msgs').add([EE('span', msg), EE('br')]);
}

function buildlog_url(host, msg) {
	var tokens = msg.match(/^([^/]+)\/(\S+) (.*)/);
	var repo = tokens[1];
	var pkgname = tokens[2];
	var pkgver = tokens[3];

	return [buildlogsUri, host, repo, pkgname, pkgname].join('/') + '-' + pkgver + '.log';
}

function sort_table(){
	var host_header = document.getElementById( 'host' ).getElementsByTagName( 'a' )[0];
	if (host_header) host_header.click(); // this will sort table by host column

	// update order nr's after sorting
	$('#servers tr').each(function(obj, index) {
		$('#'+obj.id+' .nr').ht(index+1);
	});
}

window.addEventListener("load", connect, false);
